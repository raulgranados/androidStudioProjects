package org.escoladeltreball.iam43533099.retrofitv1;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by iam43533099 on 1/26/16.
 */
public interface MiInterficie {



        //baseURL=http://www.omdbapi.com/
        //Ejemplo de busqueda por titulo
        //ORIGINAL:
        // http://www.omdbapi.com/?s=red&r=json

        //  http://www.omdbapi.com/    s=red r=json
        // donde S es el titulo que buscaremos y R el tipo de documento
        //cuando llamamos a http://www.omdbapi.com/?s=red&r=json obtenemos un objeto search
        @GET("/")
        Call<Busqueda> listaPeliculas(@Query("s") String Title,@Query("r") String type );


}

