package org.escoladeltreball.iam43533099.retrofitv1;

import android.app.Activity;
import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.Context;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.system.Os;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends ListActivity implements View.OnClickListener, AdapterView.OnItemClickListener{



    EditText eText;
    Button btn;
    ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button b = (Button) findViewById(R.id.button);
        b.setOnClickListener(this);

        eText = (EditText) findViewById(R.id.edittext);

    }
    public void test() {

        final String titulo = eText.getText().toString();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://www.omdbapi.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MiInterficie service = retrofit.create(MiInterficie.class);

        service.listaPeliculas(titulo, "JSON").enqueue(new Callback<Busqueda>() {

            @Override
            public void onResponse(Response<Busqueda> response) {


                ArrayList<String> titolsPelis = new ArrayList<String>();

                for (int i = 0; i < response.body().getSearch().size() - 1; i++) {

                    titolsPelis.add(response.body().getSearch().get(i).getTitle());

                }

               // ListView lista = (ListView) findViewById(R.id.list);

                adapter = new ArrayAdapter<String>(getApplication(), android.R.layout.simple_list_item_1, titolsPelis);

                //lista.;
                        setListAdapter(adapter);

            }

            @Override
            public void onFailure(Throwable t) {

                Log.d("ERROR", "ERROR");
            }
        });
    }


    @Override
    public void onClick(View v) {
        test();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(this,"item clicado",Toast.LENGTH_LONG).show();
    }
}
