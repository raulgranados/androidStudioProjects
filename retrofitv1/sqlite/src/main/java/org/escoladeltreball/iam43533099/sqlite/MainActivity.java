package org.escoladeltreball.iam43533099.sqlite;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity {

    String nombreBD = "musica.db";
    SQLiteDatabase mydatabase;
    String africa = "CREATE TABLE IF NOT EXISTS africa( _id INTEGER PRIMARY KEY AUTOINCREMENT, titulo TEXT, autor TEXT, pais TEXT, any INTEGER )";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StringBuilder sb= new StringBuilder();
        mydatabase = openOrCreateDatabase(nombreBD,MODE_PRIVATE, null);
        mydatabase.execSQL(africa);

        Log.i(this.getClass().toString(), "Tabla africa creada");

        mydatabase.execSQL("INSERT INTO africa (titulo, autor, pais, any) VALUES ('My people' ,'Youssou N Dour', 'Senegal', 1994)");
        mydatabase.execSQL("INSERT INTO africa (titulo, autor, pais, any) VALUES ( 'Vuka Vuka', 'The Manhattan Brothers', 'South Africa', 1954)");
        mydatabase.execSQL("INSERT INTO africa (titulo, autor, pais, any) VALUES ( 'Mother of Hope', 'Ladysmith Black Mambazo', 'South Africa', 1973)");
        mydatabase.execSQL("INSERT INTO africa (titulo, autor, pais, any) VALUES ('Desert roots', 'Hamid Baroudi', 'Argelia', 1994)");

        Log.i(this.getClass().toString(), "Inserts realizados");

        Cursor c = mydatabase.rawQuery(" SELECT * FROM africa ", null);

        //Nos aseguramos de que existe al menos un registro
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {
                int _id = c.getInt(0);
                String titulo = c.getString(1);
                String autor = c.getString(2);
                String pais = c.getString(3);
                int any = c.getInt(4);

                sb.append(Integer.toString(_id)+"\n").append(titulo+"\n").append(autor+"\n").append(pais+"\n").append(Integer.toString(any)+"\n");


            } while(c.moveToNext());
        }

        Log.i(this.getClass().toString(), String.valueOf(sb));


    }
}
