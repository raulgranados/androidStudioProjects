package com.example.vimet.myapplication;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity implements View.OnClickListener {


    final String BREAK_LINE = System.getProperty("line.separator");
    final Uri CONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
    final String[] INFO_CONTACTS = new String[]{
            ContactsContract.Contacts.TIMES_CONTACTED,
            ContactsContract.Contacts.CONTACT_STATUS,
            ContactsContract.Contacts.PHONETIC_NAME,
            ContactsContract.Contacts.PHONETIC_NAME_STYLE,
            //ContactsContract.Data.IS_USER_PROFILE,
            ContactsContract.Contacts.LOOKUP_KEY,
            //ContactsContract.PhoneLookup.LABEL
            ContactsContract.Contacts.CONTACT_STATUS_ICON,
            ContactsContract.Contacts.LAST_TIME_CONTACTED,
            ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP,
            ContactsContract.Contacts._ID,
            ContactsContract.Contacts.PINNED,
            ContactsContract.Contacts.DISPLAY_NAME_SOURCE,
            ContactsContract.Contacts.PHOTO_URI,
            ContactsContract.Contacts.PHOTO_THUMBNAIL_URI,
            ContactsContract.Contacts.CONTACT_CHAT_CAPABILITY,
            ContactsContract.Contacts.PHOTO_ID,
            ContactsContract.Contacts.SEND_TO_VOICEMAIL,
            ContactsContract.Contacts.CUSTOM_RINGTONE,
            ContactsContract.Contacts.NAME_RAW_CONTACT_ID,
            ContactsContract.Contacts.PHOTO_FILE_ID,
            ContactsContract.Contacts.HAS_PHONE_NUMBER,
            ContactsContract.Contacts.CONTACT_STATUS_LABEL,
            //phoneBook_bucket
            ContactsContract.Contacts.DISPLAY_NAME,
            //phoneBook_Bucket_Alt
            ContactsContract.Contacts.SORT_KEY_ALTERNATIVE,
            //phoneBook_Label
            ContactsContract.Contacts.IN_VISIBLE_GROUP,
            ContactsContract.Contacts.STARRED,
            ContactsContract.Contacts.DISPLAY_NAME_ALTERNATIVE,
            ContactsContract.Contacts.SORT_KEY_PRIMARY,
            ContactsContract.Contacts.CONTACT_PRESENCE,
            ContactsContract.Contacts.CONTACT_STATUS_RES_PACKAGE,
            ContactsContract.Contacts.CONTACT_STATUS_TIMESTAMP
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Button displayContactInfo = (Button) findViewById(R.id.infoGenericaBTN);
        displayContactInfo.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        ContentResolver contentResolver = getApplicationContext().getContentResolver();

        Cursor c = contentResolver.query(CONTENT_URI, INFO_CONTACTS, null, null, null);
        int index = 0;
        String infoContact = "";

        if (c.moveToFirst()) {
            while (c.moveToNext()) {

                infoContact = new StringBuilder()
                        .append("CONTACTE: "+(index)+BREAK_LINE)
                        .append("times_contacted: " + c.getString(c.getColumnIndex(INFO_CONTACTS[0])) + BREAK_LINE)
                        .append("contact_status: " + c.getString(c.getColumnIndex(INFO_CONTACTS[1])) + BREAK_LINE)
                        .append("phonetic_name: " + c.getString(c.getColumnIndex(INFO_CONTACTS[2])) + BREAK_LINE)
                        .append("phonetic_name_style: " + c.getString(c.getColumnIndex(INFO_CONTACTS[3])) + BREAK_LINE)
                                //  .append("is_user_profile: " + c.getString(c.getColumnIndex(INFO_CONTACTS[4])) + BREAK_LINE)
                        .append("lookup: " + c.getString(c.getColumnIndex(INFO_CONTACTS[4])) + BREAK_LINE)
                                //  .append("phonebook_label_alt: "+c.getString(c.getColumnIndex(INFO_CONTACTS[6]))+BREAK_LINE) //)
                        .append("contact_status_icon: " + c.getString(c.getColumnIndex(INFO_CONTACTS[5])) + BREAK_LINE)
                        .append("last_time_contacted: " + c.getString(c.getColumnIndex(INFO_CONTACTS[6])) + BREAK_LINE)
                        .append("contact_last_updated_timestamp: " + c.getString(c.getColumnIndex(INFO_CONTACTS[7])) + BREAK_LINE)
                        .append("_id: " + c.getString(c.getColumnIndex(INFO_CONTACTS[8])) + BREAK_LINE)
                        .append("pinned: " + c.getString(c.getColumnIndex(INFO_CONTACTS[9])) + BREAK_LINE)
                        .append("display_name_source: " + c.getString(c.getColumnIndex(INFO_CONTACTS[10])) + BREAK_LINE)
                        .append("photo_uri: " + c.getString(c.getColumnIndex(INFO_CONTACTS[11])) + BREAK_LINE)
                        .append("photo_thum_uri: " + c.getString(c.getColumnIndex(INFO_CONTACTS[12])) + BREAK_LINE)
                        .append("contact_chat_capability: " + c.getString(c.getColumnIndex(INFO_CONTACTS[13])) + BREAK_LINE)
                        .append("photo_id: " + c.getString(c.getColumnIndex(INFO_CONTACTS[14])) + BREAK_LINE)
                        .append("send_to_voicemail: " + c.getString(c.getColumnIndex(INFO_CONTACTS[15])) + BREAK_LINE)
                        .append("custom_ringtone: " + c.getString(c.getColumnIndex(INFO_CONTACTS[16])) + BREAK_LINE)
                        .append("name_raw_contact_id: " + c.getString(c.getColumnIndex(INFO_CONTACTS[17])) + BREAK_LINE)
                        .append("photo_file_id: " + c.getString(c.getColumnIndex(INFO_CONTACTS[18])) + BREAK_LINE)
                        .append("has_phone_number: " + c.getString(c.getColumnIndex(INFO_CONTACTS[19])) + BREAK_LINE)
                        .append("contact_status_label: " + c.getString(c.getColumnIndex(INFO_CONTACTS[20])) + BREAK_LINE)
                                //  .append("phonebook_bucket: " + c.getString(c.getColumnIndex(INFO_CONTACTS[23])) + BREAK_LINE)
                        .append("display_name: " + c.getString(c.getColumnIndex(INFO_CONTACTS[21])) + BREAK_LINE)
                                //  .append("phonebook_bucket_alt: " + c.getString(c.getColumnIndex(INFO_CONTACTS[25])) + BREAK_LINE)
                        .append("sort_key_alt: " + c.getString(c.getColumnIndex(INFO_CONTACTS[22])) + BREAK_LINE)
                                //  .append("phonebook_label: " + c.getString(c.getColumnIndex(INFO_CONTACTS[27])) + BREAK_LINE)
                        .append("in_visible_group: " + c.getString(c.getColumnIndex(INFO_CONTACTS[23])) + BREAK_LINE)
                        .append("starred: " + c.getString(c.getColumnIndex(INFO_CONTACTS[24])) + BREAK_LINE)
                        .append("display_name_alt: " + c.getString(c.getColumnIndex(INFO_CONTACTS[25])) + BREAK_LINE)
                        .append("sort_key: " + c.getString(c.getColumnIndex(INFO_CONTACTS[26])) + BREAK_LINE)
                        .append("contact_presence: " + c.getString(c.getColumnIndex(INFO_CONTACTS[27])) + BREAK_LINE)
                        .append("contact_status_res_package: " + c.getString(c.getColumnIndex(INFO_CONTACTS[28])) + BREAK_LINE)
                        .append("contact_status_ts: " + c.getString(c.getColumnIndex(INFO_CONTACTS[29])) + BREAK_LINE + BREAK_LINE)
                        .toString();

                index++;

            }
        }

        c.close();

        Intent intent = new Intent(this,InformacioGenerica.class);
        intent.putExtra("InfoToWrite",infoContact);
        startActivity(intent);
    }
}