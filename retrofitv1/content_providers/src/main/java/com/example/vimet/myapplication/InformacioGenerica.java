package com.example.vimet.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

/**
 * Created by vimet on 07/03/16.
 */
public class InformacioGenerica extends Activity {

    final String BREAK_LINE = System.getProperty("line.separator");



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infogen);


        Intent intent = getIntent();

//        String info = intent.getStringExtra("InfoToWrite");
//
//        TextView textView = (TextView) findViewById(R.id.infoGenerica);
//        textView.setTextSize(20);
//        textView.setText(info);

//        TreeMap<Integer, String> map = new TreeMap<Integer, String>((Map<Integer, String>) getIntent().getExtras().get("map"));


        StringBuilder campContactes = new StringBuilder();
        campContactes.append("CAMPS DE CONTACTES"+BREAK_LINE)
                .append("0:times_contacted" + BREAK_LINE)
                .append("1:contact_status" + BREAK_LINE)
                .append("2:phonetic_name" + BREAK_LINE)
                .append("3:phonetic_name_style" + BREAK_LINE)
                .append("4:is_user_profile" + BREAK_LINE)
                .append("5:lookup" + BREAK_LINE)
                .append("6:phonebook_label_alt" + BREAK_LINE)
                .append("7:contacts_status_icon" + BREAK_LINE)
    .append("8:last_time_contacted" + BREAK_LINE)
    .append("9:contact_last_update" + BREAK_LINE)
    .append("10:_id" + BREAK_LINE)
    .append("11:pinned" + BREAK_LINE)
    .append("12:display_name_source" + BREAK_LINE)
    .append("13:photo_uri" + BREAK_LINE)
    .append("14:photo_thumb_uri" + BREAK_LINE)
    .append("15:contact_chat_capability" + BREAK_LINE)
    .append("16:photo_id" + BREAK_LINE)
    .append("17:send_to_voicemail" + BREAK_LINE)
    .append("18:custom_ringtone" + BREAK_LINE)
    .append("19:name_raw_contact_id" + BREAK_LINE)
    .append("20:photo_file_id" + BREAK_LINE)
    .append("21:has_phone_name" + BREAK_LINE)
    .append("22:contact_status_label" + BREAK_LINE)
    .append("23:phonebook_bucket" + BREAK_LINE)
    .append("24:display_name" + BREAK_LINE)
    .append("25:phonebook_bucket_alt" + BREAK_LINE)
    .append("26:sort_key_alt" + BREAK_LINE)
    .append("27:phonebook_label" + BREAK_LINE)
    .append("28:in_visible_group" + BREAK_LINE)
    .append("29:starred" + BREAK_LINE)
    .append("30:display_name_alt" + BREAK_LINE)
    .append("31:sort_key" + BREAK_LINE)
    .append("32:contact_presence"  + BREAK_LINE)
    .append("33:contact_status_res_package"  + BREAK_LINE)
    .append("34:contact_status_ts"  + BREAK_LINE)
    .toString();

    String contact = intent.getStringExtra("contact");

    TextView contactes = (TextView) findViewById(R.id.contactes);
    contactes.setTextSize(25);
    contactes.setText(campContactes+"CONTACTES\n"+contact);

    contactes.setMovementMethod(new ScrollingMovementMethod());
//        textView.setMovementMethod(new ScrollingMovementMethod());
}


}
