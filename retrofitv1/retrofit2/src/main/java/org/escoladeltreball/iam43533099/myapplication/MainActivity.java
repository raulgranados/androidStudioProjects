package org.escoladeltreball.iam43533099.myapplication;

import android.app.Activity;
import android.app.ListActivity;
import android.os.Bundle;
import android.system.Os;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends ListActivity{

    ArrayAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //192.168.2.40
        //http://localhost:8080/Practica2/ApiServlet
        test();
    }

    public void test() {

        //final String titulo = eText.getText().toString();

        Retrofit retrofit = new Retrofit.Builder()
                //.baseUrl("http://localhost:8080/Practica2/ApiServlet") 192.168.2.40  WIAM2 clase12345
                .baseUrl("http://192.168.2.40:8080/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MyInterface service = retrofit.create(MyInterface.class);

        service.listaPeliculas().enqueue(new Callback<List<Ticket>>() {
            @Override
            public void onResponse(Response<List<Ticket>> response) {

                ArrayList<String> listaTickets = new ArrayList<String>();

                for (int i = 0; i < response.body().size(); i++) {

                    listaTickets.add("CLIENTE: "+response.body().get(i).getCliente()+
                                        "\nTITULO: "+response.body().get(i).getTitulo()+
                                            "\nDESCRIPCION: "+response.body().get(i).getDescripcion());

                }

                // ListView lista = (ListView) findViewById(R.id.list);

                adapter = new ArrayAdapter<String>(getApplication(), android.R.layout.simple_list_item_1, listaTickets);

                //lista.;
                setListAdapter(adapter);

            }
                //Toast.makeText(MainActivity.this, "la resposata es:"+response.body().size(), Toast.LENGTH_LONG ).show();


            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_LONG ).show();
            }
        });



    }


}
