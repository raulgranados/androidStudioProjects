package org.escoladeltreball.iam43533099.myapplication;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by iam43533099 on 2/19/16.
 */
public interface MyInterface {

    @GET("/Practica2/ApiServlet")
    Call<List<Ticket>> listaPeliculas();

}
