package org.escoladeltreball.iam43533099.myapplication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iam43533099 on 2/19/16.
 */
public class Ticket {

    @SerializedName("cliente")
    String cliente;
    @SerializedName("titulo")
    String titulo;
    @SerializedName("descripcion")
    String descripcion;
    int numDeTicket;

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getNumDeTicket() {
        return numDeTicket;
    }

    public void setNumDeTicket(int numDeTicket) {
        this.numDeTicket = numDeTicket;
    }



}
