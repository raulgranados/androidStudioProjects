package org.escoladeltreball.iam43533099.contentproviders;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

/**
 * Created by iam43533099 on 2/22/16.
 */
public class ClientesProvider extends ContentProvider {

    //DEFINICION DEL CONTENT URI
    private static final String uri =
            "contetnt://org.escoladeltreball.iam43533099.contentproviders/clientes";

    public static final Uri CONTENT_URI = Uri.parse(uri);

    //Necesario para UriMatcher
    private static final int CLIENTES = 1;
    private static final int CLIENTES_ID = 2;
    private static final UriMatcher uriMatcher;

    //ATRIBUTOS BASE DE DATOS PARA ALMACENAR NOMBRE, VERSION Y TABLA A LA QUE A
    //ACCEDERA NUESTRO CONTENT PROVIDER
    private static ClientesSqliteHelper clidbh;
    private static final String BD_NOMBRE = "DBClientes";
    private static final int BD_VERSION = 1;
    private static final String TABLA_CLIENTES = "Clientes";

    //Inicializamos el UriMatcher
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI("org.escoladeltreball.iam43533099.contentproviders", "clientes", CLIENTES);
        uriMatcher.addURI("org.escoladeltreball.iam43533099.contentproviders", "clientes/#", CLIENTES_ID);
    }

    //En este metodo inicializamos la base de datos
    @Override
    public boolean onCreate() {

        clidbh = new ClientesSqliteHelper(getContext(), BD_NOMBRE, null, BD_VERSION);
        return true;
    }

    //Este método recibe como parámetros una URI,
    //una lista de nombres de columna, un criterio de selección,
    // una lista de valores para las variables utilizadas en el criterio anterior,
    // y un criterio de ordenación.

    //El método query deberá devolver los datos solicitados según la URI indicada,

    // Si la URI hace referencia a un cliente concreto por su ID ése deberá ser el único registro devuelto.
    // Si por el contrario es un acceso genérico a la tabla de clientes
    // habrá que realizar la consulta SQL correspondiente a la base de datos
    // respetanto los criterios pasados como parámetro.
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        //Si es una consulta a un ID concreto construimos el WHERE
        String where = selection;
        if(uriMatcher.match(uri) == CLIENTES_ID){
            where = "_id=" + uri.getLastPathSegment();
        }

        SQLiteDatabase db = clidbh.getWritableDatabase();

        Cursor c = db.query(TABLA_CLIENTES, projection, where,
                selectionArgs, null, null, sortOrder);

        return c;
    }

    //Existirán dos tipos MIME distintos para cada entidad del content provider,
    // uno de ellos destinado a cuando se devuelve una lista de registros como resultado,
    // y otro para cuando se devuelve un registro único concreto. De esta forma,
    // seguiremos los siguientes patrones para definir uno y otro tipo de datos:

    //      “vnd.android.cursor.item/vnd.xxxxxx” –> Registro único
    //      “vnd.android.cursor.dir/vnd.xxxxxx” –> Lista de registros


    @Override
    public String getType(Uri uri) {
        int match = uriMatcher.match(uri);

        switch (match)
        {
            case CLIENTES:
                return "vnd.android.cursor.dir/vnd.sgoliver.cliente";
            case CLIENTES_ID:
                return "vnd.android.cursor.item/vnd.sgoliver.cliente";
            default:
                return null;
        }
    }

    // El método insert() sí es algo diferente, aunque igual de sencillo.
    // La diferencia en este caso radica en que debe devolver
    // la URI que hace referencia al nuevo registro insertado.
    // Para ello, obtendremos el nuevo ID del elemento insertado como resultado del método insert() de SQLiteDatabase,
    // y posteriormente construiremos la nueva URI mediante el método auxiliar ContentUris.withAppendedId()
    // que recibe como parámetro la URI de nuestro content provider y el ID del nuevo elemento.
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long regId = 1;

        SQLiteDatabase db = clidbh.getWritableDatabase();

        regId = db.insert(TABLA_CLIENTES, null, values);

        Uri newUri = ContentUris.withAppendedId(CONTENT_URI, regId);

        return newUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int cont;

        //Si es una consulta a un ID concreto construimos el WHERE
        String where = selection;
        if(uriMatcher.match(uri) == CLIENTES_ID){
            where = "_id=" + uri.getLastPathSegment();
        }

        SQLiteDatabase db = clidbh.getWritableDatabase();

        cont = db.delete(TABLA_CLIENTES, where, selectionArgs);

        return cont;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int cont;

        //Si es una consulta a un ID concreto construimos el WHERE
        String where = selection;
        if(uriMatcher.match(uri) == CLIENTES_ID){
            where = "_id=" + uri.getLastPathSegment();
        }

        SQLiteDatabase db = clidbh.getWritableDatabase();

        cont = db.update(TABLA_CLIENTES, values, where, selectionArgs);

        return cont;
    }
}
