package org.escoladeltreball.iam43533099.contentproviders;

import android.provider.BaseColumns;

/**
 * Created by iam43533099 on 2/22/16.
 */
public class Clientes implements BaseColumns {

    private Clientes() {}

    //NOMBRE DE LAS COLUMNAS
    public static final String COL_NOMBRE = "nombre";
    public static final String COL_TELEFONO = "telefono";
    public static final String COL_EMAIL = "email";


}
