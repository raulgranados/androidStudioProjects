package org.escoladeltreball.jamoros.buttons;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.ToggleButton;

import static org.escoladeltreball.jamoros.buttons.R.id.button5;
import static org.escoladeltreball.jamoros.buttons.R.id.button6;
import static org.escoladeltreball.jamoros.buttons.R.id.mySwitch;
import static org.escoladeltreball.jamoros.buttons.R.id.toggleButton;

public class MainActivity extends Activity implements OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Recordem que per manegar els esdeveniments del botó 3,
        // necessitem tenir una referència del botó 3 i registrar un escoltador
        // dels esdeveniments adients, "clicks" per exemple,
        // això últim es fa amb el mètode setOnClickListener

        Button btn3 = (Button) findViewById(R.id.button3);
        btn3.setOnClickListener(this); // Aquesta classe que instanciarem,this:
        // MainActivity, implementa la interfície OnClickListener
        // per tant la puc fer servir com a escoltadora de "onclicks"
        // sobre el botó 3

        Button btn4 = (Button) findViewById(R.id.button4);

        // Registrem l'escoltador amb una clase interna anònima que definim:
        btn4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                Toast.makeText(
                        MainActivity.this,
                        ((Button) view).getText()
                                + " was clicked using an anonymous class !",
                        Toast.LENGTH_SHORT).show();
            }
        });

        //Ejercicio3: obtenemos la referència del botó 5 y registramos un Listener

        Button btn5 = (Button) findViewById(button5);
        btn5.setOnClickListener(btnListener);

        Button btn6 = (Button) findViewById(button6);
        btn6.setOnClickListener(btnListener);


        ToggleButton toggleButton=(ToggleButton) findViewById(R.id.toggleButton);
        toggleButton.setOnClickListener(this);



            if (Build.VERSION.SDK_INT>=14) {

            Switch mySwitch = (Switch) findViewById(R.id.mySwitch);
            mySwitch.setOnClickListener(this);

            //set the switch to ON
            mySwitch.setChecked(true);

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Ejercicio3: implementamos el metodo onClik() para que muestre el toast
    @Override
    public void onClick(View v) {

        Button btn = (Button) v;

        if (v.getId()==R.id.button3) {

            Toast.makeText(this, btn.getText() + " Button 3  was clicked using an Activity class Listener! ",
                    Toast.LENGTH_SHORT).show();
        } else

            if ( v.getId()==R.id.toggleButton) {

                if (((ToggleButton)v).isChecked()) {
                    Toast.makeText(MainActivity.this, "i'm on",
                        Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "i'm off",
                        Toast.LENGTH_SHORT).show();
                }

            } else

                if (v.getId()==R.id.mySwitch) {

                    if (((Switch)v).isChecked()) {
                        Toast.makeText(MainActivity.this, "switch on", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MainActivity.this, "switch off", Toast.LENGTH_SHORT).show();
                    }
                }
            }




    	/*
     * Aquesta és una de les maneres de relacionar l'esdeveniment (en aquest cas
	 * un click de ratolí) sobre una view (en aquest cas un Button) amb l'acció
	 * conseqüència que esdevindrà després
	 */


    public void myClickHandler(View view) {

        // Afegeix codi NOMÉS AQUÍ de manera que si clickem el primer botó afegeixi el
        // toast "Welcome to the Buttons Show"

        Button btn = (Button) view;

        // Ejercicio 1: Para seleccionar el boton_1, obtenemos el texto de la view y lo comparamos
        // con el texto del boton_1 ( obtenido a traves de android:text="@string/button_1" del
        // activity_main que nos lleva a res/values/strings.xml, ahi se encuentra el texto del
        // boton_1: android:text="@string/button_1" ).

        //if (btn.getText().equals("Button 1")) {
        //    Toast.makeText(this, btn.getText() + "Welcome to the Buttons Show",
        //          Toast.LENGTH_SHORT).show();


        // Ejercicio 2: Referenciamos de forma univoca el boton_1 a traves de su id. Añadimos la
        // definicioón en el activity_main.xml.

        if (btn.getId()==R.id.button1) {

            Toast.makeText(this, btn.getText() + " Welcome to the Buttons Show",
                    Toast.LENGTH_SHORT).show();
            Toast.makeText(this, btn.getText() + " was clicked!",
                    Toast.LENGTH_SHORT).show();

        } else {

            Toast.makeText(this, btn.getText() + " was clicked!",
                    Toast.LENGTH_SHORT).show();
        }
    }


    // Crearem una variable de tipus interfície al qual li assignem un objecte (una classe interna)
    // que implementa OnclickListener (escoltadora de clicks de botons)
    // Al tenir una referència apuntant a la classe anònima, la podem reutilitzar per altres botons

    private OnClickListener btnListener = new OnClickListener() {

        public void onClick(View view) {

            Button btn = (Button) view;

            try{

                if (view.getId()== R.id.button5) {

                    Toast.makeText(MainActivity.this, btn.getText() + " Button 5  was clicked using a reference to an anonymous class ! ",
                        Toast.LENGTH_SHORT).show();

                } else if (view.getId()== R.id.button6) {

                    Toast.makeText(MainActivity.this, btn.getText() + " Button 6  was clicked (reusing code!!!) using a reference to an anonymous class ! ",
                        Toast.LENGTH_SHORT).show();

                }

            }catch (Exception e){
                   //NOTHING
            }


        }
    };

}
