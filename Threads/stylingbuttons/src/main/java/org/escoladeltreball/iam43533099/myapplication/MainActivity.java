package org.escoladeltreball.iam43533099.myapplication;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton imgBtn = (Button) findViewById(R.id.);
        imgBtn.setOnClickListener((View.OnClickListener) this);
    }

    @Override
    public void onClick(View v) {

        ImageButton imgBtn = (ImageButton) v;

        if (v.getId()==R.id.myButton) {

            Toast.makeText(this, imgBtn.getText() + " Button 3  was clicked using an Activity class Listener! ",
                    Toast.LENGTH_SHORT).show();
        }

    }
}
