package org.escoladeltreball.jamoros.yambav0;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import winterwell.jtwitter.Twitter;


// Volem obligar a que aquesta classe sigui escoltadora (listener)
// d'un esdeveniment molt concret (el click que es fa amb el ratolí).
// Això tindrà com a conseqüència que la classe disposi d'un mètode
// on es descriu l'acció a realitzar un cop es produeix l'esdeveniment
public class StatusActivity extends Activity implements View.OnClickListener {
    //constant que utilitzarem al mètode Log.d( , )
    private static final String TAG = "StatusActivity";
    private String oldMessage = "Esperem que l'usuari no possi aquest mateix" +
            " missatge al principi o l'app no funcionarà";
    Twitter twitter;
    EditText editText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Cridem al mètode de la superclasse
        super.onCreate(savedInstanceState);

        // A partir del layout manager definit en codi XML  generem els
        // objecte Java adients, d'això se'n diu “inflate XML”
        setContentView(R.layout.activity_status);

        // Trobem les Views que utilitzarem a partir del seu id
        Button button = (Button) findViewById(R.id.buttonUpdate);
        editText = (EditText) findViewById(R.id.message_text);

        // Registrem el botó per a notificar quan és clickat
        button.setOnClickListener(this);

        // Ens connectem al servei online que suporta l'API de Yamba (un lloc «tipus» Twitter que utilitza Status.net)
        // No ens fixem que hem posat hardcoded l'usuari i el password
        twitter = new Twitter("student", "password");
        //ja no funciona la url antiga de marakana, ja que Twitter ha comprat el domini i l'ha canviat
        //twitter.setAPIRootUrl("http://yamba.marakana.com/api");
        twitter.setAPIRootUrl("http://yamba.newcircle.com/api");
    }




    // Es crida quan el botó es premut
    @Override
    public void onClick(View v) {
        // Capturem el missatge
  /*      String message = editText.getText().toString();
        Toast.makeText(this, message + "" , Toast.LENGTH_SHORT).show();
        Log.d(TAG, "onClicked with message:" + message);

        //Fem que l'API del servei web actualitzi el nostre status a Yamba
        twitter.setStatus(message);
*/
        String message = editText.getText().toString();

        EjemploAsyncTask tarea2 = new EjemploAsyncTask();
        tarea2.execute(message);

    }

    //APARTADO 2)

    // Creamos la clase AsynTask para realizar la tarea en segundo plano, recibira como parametros
    // un string que contiene el mensaje que recibe y necesita el metodo doInBackground(), un void ya que no analizaremo
    // el progreso de la tarea y por último, otro void, ya que nuestro método no devuelve nada.
    private class EjemploAsyncTask extends AsyncTask<String,Void,Void>{

        // APARTADO 3)Sobreescribimos el método onPreExectute() para mostrar un mensaje.
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // (*)
            // Llamamos al método runOnUiThread() para “enviar” operaciones al hilo principal desde el hilo secundario
            // Le pasamos como parámetro un objeto Runnable
            // Implementamos su método run()
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(StatusActivity.this, "Estamos en el método onPreExecute()",
                            Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        protected Void doInBackground(String... params) {

            final String message =  params[0];
            if (message.equals("")) { // Si la cadena és buida no actualitzis i mostra missatge
                //(*)
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(StatusActivity.this, "No pot ser la cadena buida",
                                Toast.LENGTH_SHORT).show();
                    }
                });
            } else if (message.equals(oldMessage)) { // Si repeteixes missatge, no actualitzis i mostra missatge
              //  (*)
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(StatusActivity.this, "No es pot repetir la cadena",
                                Toast.LENGTH_SHORT).show();
                    }
                });

            } else {
                twitter.setStatus(message);
                oldMessage = message;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(StatusActivity.this, message,
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
            // Fem un log cada cop que clickem el botó (podríem diferenciar si la
            // cadena es buida o no)
            Log.d(TAG, "onClicked");

            return null;


        }
      /*     @Override
        protected void  doInBackground(String... params) {
            String message =  params[0];
            if (message.equals("")) { // Si la cadena és buida no actualitzis i mostra missatge
                Toast.makeText(StatusActivity.this, "introdueix una cadena no buida",
                        Toast.LENGTH_SHORT).show();
            } else if (message.equals(oldMessage)) { // Si repeteixes missatge, no actualitzis i mostra missatge
                Toast.makeText(StatusActivity.this, "No es pot repetir la cadena",
                        Toast.LENGTH_SHORT).show();
            } else {
                twitter.setStatus(message);
                oldMessage = message;
            }
            // Fem un log cada cop que clickem el botó (podríem diferenciar si la
            // cadena es buida o no)
            Log.d(TAG, "onClicked");

        }*/
    }
}
