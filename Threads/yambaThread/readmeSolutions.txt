Possibles errors:

1) No hem col·locat bé les llibreries necessàries: jtwitter.jar
Exception al logcat ClassNotFoundException

Afegir manualment, desde fora de l'android studio, les llibreries al directori libs del projecte
(si no existeix el directori libs crear-lo).
Si Android Studio no detecta les llibreries (fer un refresh per si de cas),
amb el botó dret sortirà una opció al menú contextual per afegir com a llibreria.


2) No hem posat permisos per sortit a internet. A l'Android Manifest file hem d'afegir 
<uses-permission android:name="android.permission.INTERNET" />


3) Si no tenim internet podem trobar un missatge semblant a quan no tenim permissos d'internet:
UnknownHostException


4) Si trobem un missatge del tipus:
Received authentication challenge is null
Llavors és perquè o l'usuari o el password no són correctes.


5) Si ens salta l'excepció:
NetworkOnMainThreadException
Fer servir el fil principal (main thread) per fer una operació de xarxa estava permés fins a l'API 10. 
A partr de la 11 estem obligats a fer-ho en un altre fil(diferent del principal, el de la UI)). I en cas de no fer-ho ens salta l'excepció NetworkOnMainThreadException.
Per tant l'excepció ens ha saltat per fer servir un mòbil o emulador amb API superior a 10. 
