package org.escoladeltreball.iam43533099.yamba;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import winterwell.jtwitter.Twitter;


// Volem obligar a que aquesta classe sigui escoltadora (listener)  d'un esdeveniment molt concret (el click que es fa amb el ratolí).
// Això tindrà com a conseqüència que la classe disposi d'un mètode on es descriu l'acció a realitzar un cop es produeix l'esdeveniment
public class StatusActivity extends Activity implements View.OnClickListener {

    //constant que utilitzarem al mètode Log.d( , )
    private static final String TAG = "StatusActivity";
    //Declaramos las siguientes variables:
    String msg;
    Twitter t;
    EditText et;
    Button b;
    String ultimoMensaje;
    @Override
    public void onCreate(Bundle savedInstanceState) {

        //Cridem al mètode de la superclasse
        super.onCreate(savedInstanceState);
        //INFLATE
        // A partir del layout manager definit en codi XML  generem els objecte Java adients, d'això se'n diu “inflate XML”
        setContentView(R.layout.activity_status);
        // Trobem les Views que utilitzarem a partir del seu id
        et=((EditText)findViewById(R.id.cuadroTexto));
        // Registrem el botó per a notificar quan és clickat
        b=((Button)findViewById(R.id.button));
        b.setOnClickListener(this);
        // Ens connectem al servei online que suporta l'API de Yamba (un lloc «tipus» Twitter que utilitza Status.net)
        // No ens fixem que hem posat hardcoded l'usuari i el password
        //noinspection deprecation
        t = new Twitter("student", "password");
        t.setAPIRootUrl("http://yamba.newcircle.com/api");
    }

    @Override
    public void onClick(View v) {
    msg=et.getText().toString();

        // Es crida quan el botó es premut
        Log.d(TAG, "onClicked");
        //Fem que l'API del servei web actualitzi el nostre status a Yamba

            if((msg.length() == 0)|| (msg.equals(ultimoMensaje))) {
                Toast toast1 =Toast.makeText(getApplicationContext(),"mensaje no enviado", Toast.LENGTH_SHORT);
                toast1.show();
            }else{
                t.setStatus(msg);
                Toast toast2 =Toast.makeText(getApplicationContext(),"mensaje enviado", Toast.LENGTH_SHORT);
                toast2.show();
                ultimoMensaje=msg;
            }

            }

    }




