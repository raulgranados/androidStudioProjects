package com.example.raul.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

public class MainActivityLanzarUnaNuevaAplicacion extends Activity {

//Definimos una Key para el Intent
public final static String EXTRA_MESSAGE = "com.exemple.raul.myapplication.MESSAGE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity_lanzar_una_nueva_aplicacion);

    }

    //creamos el método senMessage(), le pasamos como parametro la view que ha de ser la pulsada

    public void sendMessage (View view){
        //necesitamos crear un Intent para empezar la segunda activity, es decir, llamar a la clase
        //SegundaActivity que sera la nueva pantalla que nos muestre el mensaje tras pulsar el botón
        Intent intent = new Intent (this, SegundaActivity.class);
        //para obtener el elemento EditText usaremos findViewById()
        EditText editText = (EditText)findViewById(R.id.edit_message);
        //una vez obtenido el elemento, guardamos su contenido, en este caso el mensaje escrito, en una variable
        String mensaje = editText.getText().toString();
        //con el metodo putExtra() añadimos el mensaje obtenido de elemento al Intent
        intent.putExtra(EXTRA_MESSAGE,mensaje);
        startActivity(intent);
    }
}
