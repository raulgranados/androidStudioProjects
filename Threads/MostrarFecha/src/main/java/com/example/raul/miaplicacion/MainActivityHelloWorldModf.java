package com.example.raul.miaplicacion;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;


// 1.- Hacemos que la clase MainActivity extienda de Activity
public class MainActivityHelloWorldModf extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity_hello_world_modf);

        // 3.- Definimos una View
        TextView tv = new TextView(this);

        // 4.- Creamos un objeto ...
        Calendar cal = new GregorianCalendar(TimeZone.getDefault());
        DateFormat date_format = DateFormat.getDateInstance(DateFormat.DEFAULT);
        // 5.- Añadimos texto al TextView con el método setText()
        tv.setText("Hello, Android developer\n+" +
                date_format.format(cal.getTime()));

        // 6.- Mostramos el TextView en nuestra Activity
        setContentView(tv);

    }
// 2.- Suprimimos todos aquellos métodos que no sean onCreate()

}
