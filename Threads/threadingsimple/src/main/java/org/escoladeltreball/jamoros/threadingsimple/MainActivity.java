package org.escoladeltreball.jamoros.threadingsimple;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener{

    private final static  int LONG_NUMBER_DELAY = 2000000000;

    private Button btnHeavyTask;
    private Button btnGreetings;
    private TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        // Fem l'inflate per crear tots els objectes que hi ha al manager layout
        setContentView(R.layout.activity_main);

        // recupero els botons
        btnHeavyTask = (Button) findViewById(R.id.button1);
        btnGreetings = (Button) findViewById(R.id.button2);

        // registro els botons: els poso la mateixa classes Activity com a
        // escoltadora d'esdeveniments 'onclick'
        btnHeavyTask.setOnClickListener(this);
        btnGreetings.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        // La view v que ha estat premuda, click, només pot ser un dels dos
        // botons que tenim
        Button b = (Button) v;

        if (b == btnHeavyTask) {
            heavyTask(); // executo una tasca pesada
            // Toast.makeText(getApplicationContext(),
            // "Tasca feixuga finalitzada !", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "Hola !",
                    Toast.LENGTH_SHORT).show();
        }

    }

    private void heavyTask() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                for (int i = 0; i < LONG_NUMBER_DELAY; i++) { // tasca pesada tonta
                }
                textView = (TextView) findViewById(R.id.textView1);
                textView.setText("Tasca Finalitzada !!!!");
            }
        }).start();

    }

}

