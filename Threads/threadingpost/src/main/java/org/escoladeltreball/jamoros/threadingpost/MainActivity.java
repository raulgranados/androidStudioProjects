package org.escoladeltreball.jamoros.threadingpost;

import android.app.Activity;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener {

    private final static int LONG_NUMBER_DELAY = 2000000000;
    private Button btnHeavyTask;
    private Button btnGreetings;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fem l'inflate per crear tots els objectes que hi ha al manager layout
        setContentView(R.layout.activity_main);

        // recupero els botons
        btnHeavyTask = (Button) findViewById(R.id.button1);
        btnGreetings = (Button) findViewById(R.id.button2);

        // registro els botons: els poso la mateixa classes Activity com a
        // escoltadora d'esdeveniments 'onclick'
        btnHeavyTask.setOnClickListener(this);
        btnGreetings.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        // La view v que ha estat premuda, click, només pot ser un dels dos
        // botons que tenim
        Button b = (Button) v;

        if (b == btnHeavyTask) {
            heavyTask(); // executo una tasca pesada

            Log.d("THREAD_INSIDE_ONCLICK",
                    "Al onClick després de cridar heavyTask!.\nEstic al fil principal ?"
                            + (Looper.getMainLooper().getThread() == Thread
                            .currentThread()));
            Log.d("THREAD_INSIDE_ONCLICK", "THREAD ID: "
                    + Thread.currentThread().getId());
            Log.d("THREAD_INSIDE_ONCLICK", "THREAD NAME: "
                    + Thread.currentThread().getName());

        } else {
            Toast.makeText(getApplicationContext(), "Hola !",
                    Toast.LENGTH_SHORT).show();
        }
    }


    private void heavyTask() {

        // Instanciant un objecte Thread creo un nou fil
        // El constructor d'aquest objecte Thread necessita com a argument
        // un objecte que implementi la interfície  Runnable.
        // Aquesta interficie representa una ordre que pot ser executada.
        // Normalment s'utilitza per executar codi en un altre fil
        // La interfície Runnable implementa el mètode run, que es a on posarem
        // el nostre codi
        new Thread(new Runnable() {

            @Override
            public void run() {
                // tasca feixuga
                for (int i = 0; i < LONG_NUMBER_DELAY; i++) { // heavy & stupid task
                }

                Log.d("THREAD_AFTER_FOR",
                        "Després de fer el bucle llarg, dintre del nou fil.\nEstic al fil principal ?"
                                + (Looper.getMainLooper().getThread() == Thread
                                .currentThread()));
                Log.d("THREAD_AFTER_FOR", "THREAD ID: "
                        + Thread.currentThread().getId());
                Log.d("THREAD_AFTER_FOR", "THREAD NAME: "
                        + Thread.currentThread().getName());

                textView = (TextView) findViewById(R.id.textView1);

                textView.post(new Runnable() {

                    @Override
                    public void run() {
                        textView.setText("Tasca Finalitzada !!!!");
                        Log.d("THREAD_INSIDE_POST",
                                "Després de canviar el TextView dintre del post.\nEstic al fil principal ?"
                                        + (Looper.getMainLooper().getThread() == Thread
                                        .currentThread()));
                        Log.d("THREAD_INSIDE_POST", "THREAD ID: "
                                + Thread.currentThread().getId());
                        Log.d("THREAD_INSIDE_POST", "THREAD NAME: "
                                + Thread.currentThread().getName());

                    }
                });

                Log.d("THREAD_AFTER_POST",
                        "Al sortir del post. Dintre del nou objecte Thread creat\nEstic al fil principal ?"
                                + (Looper.getMainLooper().getThread() == Thread
                                .currentThread()));
                Log.d("THREAD_AFTER_POST", "THREAD ID: "
                        + Thread.currentThread().getId());
                Log.d("THREAD_AFTER_POST", "THREAD NAME: "
                        + Thread.currentThread().getName());

            }
        }).start();

    }


    /*
        No mostris als alumnes com es fa amb runOnUiThread i que ho facin ells
    */




}
