package org.escoladeltreball.jamoros.threadingintrojava;

/**
 * Created by ordinari on 11/9/15.
 */
public class HelloLoopRunnable implements Runnable {
    @Override
    public void run() {
        // TODO Auto-generated method stub

        for (int i = 0; i < 1000; i++) {
            System.out.println("Hello " + i + " from a thread!");
        }
    }
}
