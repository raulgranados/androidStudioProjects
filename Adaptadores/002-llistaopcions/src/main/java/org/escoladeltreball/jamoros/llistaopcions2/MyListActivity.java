package org.escoladeltreball.jamoros.llistaopcions2;

import android.app.ListActivity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MyListActivity extends ListActivity {
    ListView LV = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_list);
        TextView TV  = (TextView) findViewById(R.id.);
        //String[] values = new String[]{"Android","", "Windows Phone", "Blackberry", "Sailfish OS", "Tizen",
                //"Firefox OS", "Ubuntu Touch", "Symbian", "Palm OS", "Maemo", "MeeGo"};
        String[] values = null;
         // First parameter - Context
        // Second parameter - Layout for the row
        // Third parameter - the Array of data
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, values);

        // Assign adapter to ListView
        setListAdapter(adapter);

        LV = getListView();

        if (values.length==0 || values.equals(null)){

        } else {
            LV.setBackgroundColor(Color.GREEN);
        }

    }

    @Override
    protected void onListItemClick(ListView l, View view, int position, long id) {
        // l The ListView where the click happened
        // v The view that was clicked within the ListView
        // position The position of the view in the list
        // id The row id of the item that was clicked

        Toast.makeText(this, "Click ListItem Number " + position, Toast.LENGTH_SHORT).show();
    }

}
