package org.escoladeltreball.iam43533099.myapplication;

import android.app.Activity;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import static android.os.Environment.getDataDirectory;
import static android.os.Environment.getExternalStorageDirectory;
import static android.os.Environment.getExternalStoragePublicDirectory;
import static android.widget.Toast.*;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getExternalFilesDir();

        String r = getDataDirectory().getAbsolutePath();

        //7.1)

        //File path = (File) getExternalStorageDirectory();

        Toast.makeText(this, "hola", LENGTH_SHORT).show();
        Toast.makeText(this, (CharSequence) getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).toString(), LENGTH_SHORT).show();
        //7.2
        getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC);
        //String ruta = f.getAbsolutePath();
        //Toast.makeText(this, ruta, Toast.LENGTH_SHORT).show();


        //7.4
        //Environment.getExternalStorageDirectory();

        //7.5
        //Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

    }


    protected void getExternalFilesDir() {

        {
            File ruta_sd = Environment.getExternalStorageDirectory();

            File f = new File(ruta_sd.getAbsolutePath(), "prueba_sd.txt");

            OutputStreamWriter fout =
                    null;
            try {
                fout = new OutputStreamWriter(
                        new FileOutputStream(f));
                fout.write("Texto de prueba.");
                fout.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception ex) {
                Log.e("Ficheros", "Error al escribir fichero a tarjeta SD");
            }


            makeText(this, (CharSequence) f.getAbsolutePath(), LENGTH_SHORT).show();
        }
    }
    //protected void getFilesDir() {

    //}

    }

