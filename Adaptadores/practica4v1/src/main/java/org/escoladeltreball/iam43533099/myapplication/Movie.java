package org.escoladeltreball.iam43533099.myapplication;

/**
 * Created by iam43533099 on 1/12/16.
 */
public class Movie {

    String titol, any;
    int posicio;

    public Movie(String titol, String   any, int posicio) {
        this.titol = titol;
        this.any = any;
        this.posicio = posicio;
    }

    public String getTitol() {
        return titol;
    }

    public void setTitol(String titol) {
        this.titol = titol;
    }

    public String getAny() {
        return any;
    }

    public void setAny(String any) {
        this.any = any;
    }

    public int getPosicio() {
        return posicio;
    }

    public void setPosicio(int posicio) {
        this.posicio = posicio;
    }
}
