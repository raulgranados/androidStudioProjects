package org.escoladeltreball.iam43533099.myapplication;

import android.app.ListActivity;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

public class MainActivity extends ListActivity implements AdapterView.OnItemClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        Resources res = getResources();
        String[] movies = res.getStringArray(R.array.movie_title);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, movies);

        // Assign adapter to ListView
        this.setListAdapter(adapter);

        // To react to selections in the list set an OnItemClickListener in our ListView.
        this.getListView().setOnItemClickListener(this);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String viewText = parent.getItemAtPosition(position).toString();
        StringBuilder info = new StringBuilder()
                .append("PARENT = " + view.getParent() + "\n")
                .append("VIEW = " + view + "\n")
                .append("VIEW's TEXT = " + viewText + "\n")
                .append("POSITION = " + position + "\n")
                .append("ID = " + id);

        Toast.makeText(getApplicationContext(), info, Toast.LENGTH_SHORT).show();
    }


}
