package org.escoladeltreball.jamoros.llistaopcions1;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

public class MyListActivity extends ListActivity implements AdapterView.OnItemClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_my_list);
        String[] values = new String[]{"Android", "iOS", "Windows Phone", "Blackberry", "Sailfish OS", "Tizen",
                "Firefox OS", "Ubuntu Touch", "Symbian", "Palm OS", "Maemo", "MeeGo"};
        // First parameter - Context
        // Second parameter - Layout for the row
        // Third parameter - the Array of data
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, values);

        // Assign adapter to ListView
        this.setListAdapter(adapter);

        // To react to selections in the list set an OnItemClickListener in our ListView.
        this.getListView().setOnItemClickListener(this);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(getApplicationContext(), "Click ListItem Number " + position, Toast.LENGTH_SHORT).show();
    }
}

/*
    Uncomment the setContentView(R.layout.activity_my_list) sentence to check which error is shown
 */