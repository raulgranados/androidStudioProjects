package org.escoladeltreball.jamoros.rssxmlpullparser;

/* POJO plain old java object */

public class New {
    private String title;
    private String link;
    private String description;
    private String guid;
    private String date;
    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }
    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    //...
}
