package org.escoladeltreball.jamoros.rssxmlpullparser;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class XmlActivity extends AppCompatActivity {


    private Button loadButton;
    private TextView resultTxt;
    private List<New> news;
    private String rssLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Inflem el layout
        setContentView(R.layout.activity_xml);


        // Recuperem els objectes inflats
        loadButton = (Button) findViewById(R.id.btnLoad);
        resultTxt = (TextView) findViewById(R.id.txtResult);


        // Registrem el botó de carrega
        loadButton.setOnClickListener(this);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_xml, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        // Amb tasca asíncrona
        LoadXmlTask task = new LoadXmlTask();

        task.execute(rssLink);
    }

    // Tasca asincrona per carregar el fitxer XML en segon pla (background)
    private class LoadXmlTask extends AsyncTask<String, Integer, Boolean> {

        protected Boolean doInBackground(String... params) {
            RssParserPull rssParser = new RssParserPull(params[0]);
            news = rssParser.parse();
            return true;
        }

        protected void onPostExecute(Boolean result) {

            // Tractem la llista de notícies
            // Per exemple: escrivim els títols en pantalla
        }

    }


}
