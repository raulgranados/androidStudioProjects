package org.escoladeltreball.iam43533099.practica4;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by iam43533099 on 1/12/16.
 */
public class Adapter extends android.widget.BaseAdapter {
    ArrayList myList = new ArrayList();
    LayoutInflater inflater;
    Context context;

    @Override
    public int getCount() {
        return myList.size();
    }

    @Override
    public Object getItem(int position) {
        return myList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public Adapter(Context context, ArrayList myList) {
        this.myList = myList;
        this.context = context;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rootView = LayoutInflater.from(context)
                .inflate(R.layout.layout_list_item, parent, false);


        TextView titulo = (TextView) rootView.findViewById(R.id.titol);
        TextView año = (TextView) rootView.findViewById(R.id.any);

        Movie pelicula = (Movie) getItem(position);
        titulo.setText(pelicula.getTitol());
        año.setText(pelicula.getAny());

        return rootView;

    }
}
