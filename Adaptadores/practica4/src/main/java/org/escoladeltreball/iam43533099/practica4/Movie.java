package org.escoladeltreball.iam43533099.practica4;

/**
 * Created by iam43533099 on 1/12/16.
 */
public class Movie {
    String titol, any;
    int posicio;

    public String getAny() {
        return any;
    }

    public void setAny(String any) {
        this.any = any;
    }

    public int getPosicio() {
        return posicio;
    }

    public void setPosicio(int  posicio) {
        this.posicio = posicio;
    }

    public String getTitol() {

        return titol;
    }

    public void setTitol(String titol) {
        this.titol = titol;
    }
}
