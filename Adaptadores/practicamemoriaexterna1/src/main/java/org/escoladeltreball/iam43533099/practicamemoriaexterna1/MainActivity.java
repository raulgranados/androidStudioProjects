package org.escoladeltreball.iam43533099.practicamemoriaexterna1;

import android.app.Activity;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //Guardamos en una variable el estado de la memoria externa
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state))

        {
            // podemos escribir y leer en la memoria externa
            Toast.makeText(this, "La memoria externa esta disponible" , Toast.LENGTH_SHORT).show();


        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state))

        {
            // solo podemos leer la memoria externa
            Toast.makeText(this, "La memoria externa esta disponible en MODO_SOLO_LECTURA" , Toast.LENGTH_SHORT).show();

        } else

        {
            // algo salio mal. no puedes leer ni escribir o no esta
            Toast.makeText(this, "ERROR!!!!, la memoria externa no esta disponible" , Toast.LENGTH_SHORT).show();

        }
    }
}